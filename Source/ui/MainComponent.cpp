/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) :  Thread ("CounterThread"),
                                                audio (audio_)
{
    startThread();
    
    
    setSize (500, 400);
    addAndMakeVisible(&gainSlider);
    gainSlider.setRange(0.0, 1.0);
    gainSlider.addListener(this);
    gainSlider.setValue(0.5);
    
    addAndMakeVisible(&timerButton);
    timerButton.setButtonText("Timer");
    timerButton.addListener(this);

}

MainComponent::~MainComponent()
{
    stopThread(500);
}

void MainComponent::resized()
{
    gainSlider.setBounds(10, 10, getWidth() - 50, 20);
    timerButton.setBounds(10, 40, 50, 30);
}


void MainComponent::run()
{
    while (!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
        std::cout << "Counter:" << Time::getApproximateMillisecondCounter() << "\n";
        Time::waitForMillisecondCounter(time + 100);
        
    }
}



void MainComponent::sliderValueChanged (Slider* slider)
{
    audio.setGain(gainSlider.getValue());
}

void MainComponent::buttonClicked (Button *)
{
    DBG("Button Clicked");
    
}



//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

