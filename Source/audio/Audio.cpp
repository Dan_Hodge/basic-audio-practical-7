/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"


Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    sharedMemory.enter();
    float fFrequency = message.getMidiNoteInHertz(message.getNoteNumber());
    oscillator.setFrequency(fFrequency);
    sharedMemory.exit();
    
    DBG("MIDI message resieved");
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    float fSampleRate = device->getCurrentSampleRate();
    oscillator.setSamplerate(fSampleRate);
    oscillator.setFrequency(440.f);
}

void Audio::audioDeviceStopped()
{
    
}


void Audio::setGain(float value)
{
    sharedMemory.enter();
    fGain = value;
    sharedMemory.exit();
}



void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
       
    
    while(numSamples--)
    {

        
        
        *outL = oscillator.getNextSample() * fGain;
        *outR = oscillator.getNextSample() * fGain;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}




