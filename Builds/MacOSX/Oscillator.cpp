//
//  Oscillator.cpp
//  JuceBasicAudio
//
//  Created by Daniel Hodge on 20/11/2014.
//
//

#include "Oscillator.h"


Oscillator::Oscillator()
{
    fAmplitude = 0.5f;
    fFrequency = 440.f;
    fPhasePosition = 0.f;
    fSampleRate = 0.f;
    
    twoPi = 2 * M_PI;
    
}


Oscillator::~Oscillator()
{
    
}


void Oscillator::setSamplerate(float sampleRate)
{
    fSampleRate = sampleRate;
}



void Oscillator::setFrequency(float frequency)
{
    fFrequency = frequency;
    fPhaseIncrement = (twoPi * fFrequency)/fSampleRate;
}


void Oscillator::setAmplitude(float amplitude)
{
    fAmplitude = amplitude;
}

///

float Oscillator::getNextSample()
{
    float output = sin(fPhasePosition) * fAmplitude;
    
    fPhasePosition += fPhaseIncrement;
    if (fPhasePosition > twoPi)
    {
        fPhasePosition -= twoPi;
    }
    
    
    
    return output;
}

