//
//  Oscillator.h
//  JuceBasicAudio
//
//  Created by Daniel Hodge on 20/11/2014.
//
//

#ifndef OSCILLATOR_H_INCLUDED
#define OSCILLATOR_H_INCLUDED

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"


class Oscillator
{
public:
    // Constructor
    Oscillator();
    
    // Destructor
    ~Oscillator();
    
    // Initialize
    void setSamplerate(float sampleRate);
    
    // Returns the next sample
    float getNextSample();
    
    // sets the oscillation frequency
    void setFrequency(float frequency);
    
    // Sets the amplitude
    void setAmplitude(float amplitude);

private:
    float fAmplitude;
    float fFrequency;
    float fPhasePosition, fPhaseIncrement;
    float fSampleRate;
    float twoPi;
};





#endif /* defined(OSCILLATOR_H_INCLUDED) */
